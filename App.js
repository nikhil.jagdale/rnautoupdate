import React, { Component } from 'react';
import { ActivityIndicator, Button, Text, View, TextInput, StyleSheet, Alert } from 'react-native';
import codePush from 'react-native-code-push';

class App extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        codePush.sync({
          installMode: codePush.InstallMode.IMMEDIATE
        })
    }

    render() {
        const isLoading = false;
        return (
            <View style={styles.container}>
                {isLoading ? <ActivityIndicator /> : (
                    <View>

                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter ID"
                            placeholderTextColor='orange'
                        />

                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter name"
                            placeholderTextColor='orange'
                        />

                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter email"
                            placeholderTextColor='orange'
                        />

                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter password"
                            placeholderTextColor='orange'
                        />

                        <View style={styles.spacing} />

                        <Button
                            title="UPDATE DETAILS"
                            style={styles.buttonStyles}
                        />

                    </View>
                )}
            </View>
        );
    }
};

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 12,
        backgroundColor: "#eaeaea"
    },
    mainRow: {
        padding: 4,
        fontWeight: "bold",
        flexDirection: 'column',
        justifyContent: 'flex-start',
        flexWrap: "nowrap",
        display: "flex",
    },
    divider_row: {
        borderWidth: 1,
        borderColor: '#e2e2e2'
    },
    spacing: {
        padding: 10
    },
    userRow: {
        fontSize: 16,
        padding: 4,
        fontWeight: "bold",
        maxWidth: "100%"
    },
    textInputStyle: {
        height: 40,
        margin: 5,
        borderColor: 'gray',
        padding: 10,
        borderWidth: 1
    },
    buttonStyles: {
        borderColor: '#20232a',
        backgroundColor: "#000055",
        width: "50%",
        color: "#20232a",
    }
})

let codePushOptions = {
  checkFrequency : codePush.CheckFrequency.ON_APP_START,
  installMode : codePush.InstallMode.IMMEDIATE
}
export default App = codePush(codePushOptions)(App);